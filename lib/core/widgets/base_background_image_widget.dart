import 'package:flutter/material.dart';

import '../resources/app_assets.dart';


class BaseBackgroundImageWidget extends StatelessWidget {
  const BaseBackgroundImageWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: 0.2,
      child: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Image.asset(
          AppImages.backgroundImage,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
