import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/widgets/base_background_image_widget.dart';

import '../resources/app_assets.dart';

class BackgroundWidget extends StatelessWidget {
  final Widget child;
  final PreferredSizeWidget? appBar;
  const BackgroundWidget({
    Key? key,
    required this.child,
    required this.appBar,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: appBar,
      backgroundColor: AppColors.kWhite,
      body: Stack(
        alignment: Alignment.center,
        children: [
          BaseBackgroundImageWidget(),
          child,
        ],
      ),
    );
  }
}
