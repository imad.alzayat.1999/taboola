import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BaseTextFieldWidget extends StatelessWidget {
  final TextEditingController textEditingController;
  final String hintText;
  final String icon;
  final bool obsText;

  const BaseTextFieldWidget({
    Key? key,
    required this.textEditingController,
    required this.hintText,
    required this.icon,
    required this.obsText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: AppSizes.s38,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(AppSizes.s19),
        border: Border.all(color: AppColors.kWageningenGreen, width: 2),
        color: AppColors.kWhite,
      ),
      child: Row(
        children: [
          Expanded(
            child: TextFormField(
              controller: textEditingController,
              obscureText: obsText,
              decoration: InputDecoration(
                hintText: hintText,
                contentPadding: const EdgeInsets.symmetric(
                  vertical: AppPaddings.p0,
                  horizontal: AppPaddings.p13,
                ),
                hintStyle: getBoldArialStyle(
                  fontSize: AppFontSizes.f14,
                  fontColor: AppColors.kWageningenGreen,
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(AppSizes.s19),
                ),
                filled: true,
                fillColor: AppColors.kWhite,
              ),
            ),
          ),
          Container(
            width: AppSizes.s55,
            height: AppSizes.s38,
            decoration: BoxDecoration(
              color: AppColors.kWageningenGreen,
              borderRadius: BorderRadius.circular(AppSizes.s15),
            ),
            child: Center(
              child: SizedBox(
                width: AppSizes.s38,
                height: AppSizes.s38,
                child: SvgPicture.asset(
                  icon,
                  color: AppColors.kWhite,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
