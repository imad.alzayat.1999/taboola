import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_routes.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../resources/app_colors.dart';

class BaseDrawer extends StatelessWidget {
  const BaseDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          SizedBox(
            width: double.infinity,
            height: AppSizes.s183,
            child: DrawerHeader(
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(AppImages.backgroundImage),
                ),
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    AppColors.kWageningenGreen.withOpacity(0.7),
                    AppColors.kTaupeGray.withOpacity(0.7),
                  ],
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: AppSizes.s80,
                    height: AppSizes.s80,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: AppColors.kCanaryYellow),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          AppImages.profileImage,
                        ),
                      ),
                    ),
                  ),
                  Text(
                    "UserName",
                    style: getRegularArialStyle(
                      fontSize: AppFontSizes.f14,
                      fontColor: AppColors.kCanaryYellow,
                    ),
                  ),
                  Text(
                    "lorem@gmail.com",
                    style: getRegularArialStyle(
                      fontSize: AppFontSizes.f14,
                      fontColor: AppColors.kCanaryYellow,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Column(
              children: [
                GestureDetector(
                  onTap: () => Navigator.pushNamed(context, AppRoutes.settingsRoute),
                  child: _getDrawerItem(
                      icon: AppIcons.settingsIcon,
                      title: AppStrings.settingsString),
                ),
                _getDivider(),
                _getDrawerItem(
                    icon: AppIcons.ordersIcon,
                    title: AppStrings.myOrdersString),
                _getDivider(),
                _getDrawerItem(
                    icon: AppIcons.pointsIcon,
                    title: AppStrings.myPointsString),
                _getDivider(),
                _getDrawerItem(
                    icon: AppIcons.chatIcon, title: AppStrings.myChatString),
                _getDivider(),
                GestureDetector(
                  onTap: () => Navigator.pushNamedAndRemoveUntil(context, AppRoutes.loginRoute, (route) => false),
                  child: _getDrawerItem(
                      icon: AppIcons.logoutIcon, title: AppStrings.logoutString),
                ),
                _getDivider(),
              ],
            ),
          ),
          Container(
            height: AppSizes.s100,
            decoration: BoxDecoration(
              color: AppColors.kBlackOlive.withOpacity(0.7),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(
                  AppImages.backgroundImage,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  /// function to design divider
  Widget _getDivider() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: AppPaddings.p16),
      child: Divider(color: AppColors.kTaupeGray),
    );
  }

  /// function to design drawer item
  Widget _getDrawerItem({required String icon, required String title}) {
    return ListTile(
      leading: SizedBox(
        height: AppSizes.s24,
        width: AppSizes.s24,
        child: SvgPicture.asset(
          icon,
          color: AppColors.kRed,
        ),
      ),
      title: Text(
        title,
        style: getBoldArialStyle(
          fontSize: AppFontSizes.f16,
          fontColor: AppColors.kDartGreen,
        ),
      ),
    );
  }
}
