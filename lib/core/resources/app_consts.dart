import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/pages/home_page/views/home_page.dart';
import 'package:ui_design/pages/menu_page/views/menu_page.dart';
import 'package:ui_design/pages/more_page/views/more_page.dart';
import 'package:ui_design/pages/profile_page/views/profile_page.dart';
import 'package:ui_design/pages/wishlist_page/views/wishlist_page.dart';

class AppConsts {
  static const int durationForIndicator = 300;
  static const int durationForButton = 450;

  static const int durationForSplashScreen = 2;

  static List<Widget> pages  = [
    MenuPage(),
    WishListPage(),
    HomePage(),
    ProfilePage(),
    MorePage(),
  ];

  static List<String> titles = ["Menu" , "WishList" , "Good Morning" , "Profile" , "More"];
  static List<String> images = [
    AppImages.profileImage,
    AppImages.profileImage,
    AppImages.profileImage,
    AppImages.profileImage,
  ];
  static const List<double> listOfPercents = [1.0 , 0.8 , 0.6 , 0.4 , 0.2];
  static const List<double> listOfRatings= [5 , 4 , 3 , 2,  1];
  static const String dummyDescription =
      "Nulla elementum hendrerit velit vitae consectetur.Praesent vulputate dui eget semper posuere. Duiselit mi, dapibus eget nisl nec, congue gravida ante.Aenean id consectetur erat. ";
  static const String loremIbsumString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
}
