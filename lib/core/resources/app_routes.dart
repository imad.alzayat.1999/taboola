class AppRoutes{
  static const String splashRoute = "/splash";
  static const String loginRoute = "/login";
  static const String registerRoute = "/register";
  static const String categoryRoute = "/category";
  static const String homeRoute = "/home";
  static const String detailsRoute = "/details";
  static const String settingsRoute = "/settings";
  static const String addressRoute = "/address";
  static const String mainRoute = "/main";
}