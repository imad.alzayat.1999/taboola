class AppSizes{
  static const double s0 = 0;
  static const double s1 = 1;
  static const double s2 = 2;
  static const double s5 = 5;
  static const double s7 = 7;
  static const double s10 = 10;
  static const double s12 = 12;
  static const double s13 = 13;
  static const double s15 = 15;
  static const double s16 = 16;
  static const double s17 = 17;
  static const double s18 = 18;
  static const double s19 = 19;
  static const double s20 = 20;
  static const double s23 = 23;
  static const double s24 = 24;
  static const double s25 = 25;
  static const double s26 = 26;
  static const double s27 = 27;
  static const double s30 = 30;
  static const double s31 = 31;
  static const double s32 = 32;
  static const double s33 = 33;
  static const double s34 = 34;
  static const double s35 = 35;
  static const double s36 = 36;
  static const double s38 = 38;
  static const double s40 = 40;
  static const double s42 = 42;
  static const double s43 = 43;
  static const double s50 = 50;
  static const double s55 = 55;
  static const double s59 = 59;
  static const double s60 = 60;
  static const double s65 = 65;
  static const double s68 = 68;
  static const double s70 = 70;
  static const double s80 = 80;
  static const double s81 = 81;
  static const double s87 = 87;
  static const double s95 = 95;
  static const double s100 = 100;
  static const double s104 = 104;
  static const double s105 = 105;
  static const double s113 = 113;
  static const double s117 = 117;
  static const double s120 = 120;
  static const double s130 = 130;
  static const double s132 = 132;
  static const double s135 = 135;
  static const double s141 = 141;
  static const double s143 = 143;
  static const double s144 = 144;
  static const double s150 = 150;
  static const double s160 = 160;
  static const double s164 = 164;
  static const double s167 = 167;
  static const double s170 = 170;
  static const double s180 = 180;
  static const double s183 = 183;
  static const double s190 = 190;
  static const double s203 = 203;
  static const double s220 = 220;
  static const double s239 = 239;
  static const double s254 = 254;
  static const double s290 = 290;
  static const double s295 = 295;
  static const double s320 = 320;
  static const double s323 = 323;
  static const double s340 = 340;
  static const double s434 = 434;
}


class AppPaddings{
  static const double p0 = 0;
  static const double p4 = 4;
  static const double p8 = 8;
  static const double p13 = 13;
  static const double p16 = 16;
  static const double p24 = 24;
  static const double p80 = 80;
}


class AppMargins{
  static const double m3 = 3;
}