import 'package:flutter/material.dart';

class AppColors{
  static const Color kWhite = Colors.white;
  static const Color kBlack = Colors.black;
  static const Color kTransparent = Colors.transparent;
  static const Color kMaximumRed = Color(0xffD52020);
  static const Color kDartGreen = Color(0xff0A3918);
  static const Color kWageningenGreen = Color(0xff3AAD26);
  static const Color kTaupeGray = Color(0xff888888);
  static const Color kOnyx = Color(0xff373737);
  static const Color kDartSilver = Color(0xff707070);
  static const Color kRed = Color(0xffFD0000);
  static const Color kIsabelline = Color(0xffF2F0F0);
  static const Color kGainsboro = Color(0xffE3DDDD);
  static const Color kBlackOlive = Color(0xff3A3636);
  static const Color kPhilippineSilver = Color(0xffB8B8B8);
  static const Color kCanaryYellow = Color(0xffFFED04);
}