class AppStrings{

  /// splash page strings
  static const String foodDeliveryString = "Food Delivery";

  /// login page strings
  static const String loginTitle = "Add Your Details To Login";
  static const String emailString = "Email";
  static const String passwordString = "Password";
  static const String loginString = "Login";

  /// signup page strings
  static const String signupSubTitle = "Add Your Details To Sign Up";
  static const String signupTitle = "Signup";
  static const String nameString = "Name";
  static const String addressString = "Address";
  static const String phoneNumberString = "Phone No.";
  static const String confirmPassString = "Confirm Password";

  /// profile page strings
  static const String goodMorningString = "Good Morning";
  static const String deliverInString = "Delivering In";
  static const String personalInformationString = "Personal Information";
  static const String editString = "Edit";
  static const String editPasswordString = "Edit Password";
  static const String editLocationString = "Edit Location";

  /// menu page strings
  static const String searchFoodString = "Search Food";

  /// more page strings
  static const String paymentDetailsString = "Payment Details";

  /// home Strings
  static const String viewAllString = "View All";
  static const String categoriesString = "Categories";
  static const String recommendedForYou = "Recommended For You";
  static const String reviewsFromCustomers = "Reviews From Customers";
  static const String ourDeliveryFeatureString = "Our Delivery Feature";

  /// details Strings
  static const String deliverTimeString = "Deliver Time";
  static const String descriptionString = "Description";
  static const String reviewsString = "Reviews";
  static const String offersString = "Offers";
  static const String ingredients = "Ingredients";
  static const String customizeYourOrder = "Customize Your Order";
  static const String addToCartString = "add to cart";
  static const String getTheOrderString = "Get The Order";
  static const String shareYourReviewString = "Share Your Review";
  static const String reviewQuestionString = "Want to review this product? \nAdd your review down below!";
  static const String addReviewString = "Add a review";
  static const String viewAllReviewsString = "View All Reviews";
  static const String selectASuitableTypeOfOrderString = "Select a suitable type of order";

  /// settings screen strings
  static const String dartModeString = "Dark Mode";
  static const String notificationString = "Notification";
  static const String languageString = "Language";
  static const String passwordSettings = "Password Settings";
  static const String settingsString = "Settings";
  static const String arabicString = "Arabic";
  static const String englishString = "English";
  static const String changePasswordString = "Change Password";
  static const String newPasswordString = "New Password";

  /// address page strings
  static const String deleteString = "delete";
  static const String addAddressString = "Add Address";

  /// main page strings
  static const String menuString = "Menu";
  static const String wishlistString = "Wishlist";
  static const String profileString = "Profile";
  static const String moreString = "More";
  static const String homeString = "Home";

  /// drawer strings
  static const String myOrdersString = "My Orders";
  static const String myPointsString = "My Points";
  static const String myChatString = "My Chat";
  static const String logoutString = "Logout";
}