import 'package:flutter/material.dart';


class AppFontFamilies{
  static const String forteFontFamily = "forte";
}

class AppFontWeights{
  static const FontWeight lightWeight = FontWeight.w300;
  static const FontWeight regularWeight = FontWeight.w400;
  static const FontWeight mediumWeight = FontWeight.w500;
  static const FontWeight semiBoldWeight = FontWeight.w600;
  static const FontWeight boldWeight = FontWeight.w700;
}

class AppFontSizes{
  static const double f6 = 6;
  static const double f8 = 8;
  static const double f10 = 10;
  static const double f12 = 12;
  static const double f14 = 14;
  static const double f16 = 16;
  static const double f20 = 20;
  static const double f24 = 24;
  static const double f26 = 26;
  static const double f28 = 28;
  static const double f30 = 30;
}