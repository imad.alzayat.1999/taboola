class AppIcons{
  static const String mainIconsPath = "assets/icons";

  /// icons
  static const String tomatoIcon = "$mainIconsPath/tomato.svg";
  static const String keyIcon = "$mainIconsPath/key.svg";
  static const String emailIcon = "$mainIconsPath/email.svg";
  static const String arrowRightIcon = "$mainIconsPath/arrow_right.svg";
  static const String phoneIcon = "$mainIconsPath/phone.svg";
  static const String userIcon = "$mainIconsPath/user.svg";
  static const String homeIcon = "$mainIconsPath/home.svg";
  static const String eyeIcon = "$mainIconsPath/eye.svg";
  static const String filterIcon = "$mainIconsPath/filter.svg";
  static const String turnRightIcon = "$mainIconsPath/turn_right.svg";
  static const String heartIcon = "$mainIconsPath/heart.svg";
  static const String plusIcon = "$mainIconsPath/plus.svg";
  static const String cartIcon = "$mainIconsPath/cart.svg";
  static const String arrowLeftIcon = "$mainIconsPath/arrow_left.svg";
  static const String clockIcon = "$mainIconsPath/clock.svg";
  static const String starIcon = "$mainIconsPath/star.svg";
  static const String minusIcon = "$mainIconsPath/minus.svg";
  static const String addressIcon = "$mainIconsPath/location.svg";
  static const String menuIcon = "$mainIconsPath/menu.svg";
  static const String moreIcon = "$mainIconsPath/more.svg";
  static const String settingsIcon = "$mainIconsPath/settings.svg";
  static const String ordersIcon = "$mainIconsPath/orders.svg";
  static const String pointsIcon = "$mainIconsPath/points.svg";
  static const String chatIcon = "$mainIconsPath/chat.svg";
  static const String logoutIcon = "$mainIconsPath/logout.svg";
}


class AppImages{
  static const String mainImagesPath = "assets/images";

  /// images
  static const String backgroundImage = "$mainImagesPath/background.png";
  static const String logoImage = "$mainImagesPath/logo.png";
  static const String profileImage = "$mainImagesPath/profile_image.jpg";
  static const String deliverImage = "$mainImagesPath/deliver.svg";
}