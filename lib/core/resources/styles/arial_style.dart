import 'package:flutter/material.dart';

import '../app_fonts.dart';

TextStyle getArialStyle({
  required double fontSize,
  required Color fontColor,
  required FontWeight fontWeight,
  required TextDecoration textDecoration,
  required double letterSpacing,
}) {
  return TextStyle(
    fontSize: fontSize,
    fontWeight: fontWeight,
    color: fontColor,
    decoration: textDecoration,
    letterSpacing: letterSpacing,
  );
}

TextStyle getLightArialStyle({
  required double fontSize,
  required Color fontColor,
  TextDecoration textDecoration = TextDecoration.none,
  double letterSpacing = 0,
}) {
  return getArialStyle(
    fontWeight: AppFontWeights.lightWeight,
    fontSize: fontSize,
    fontColor: fontColor,
    textDecoration: textDecoration,
    letterSpacing: letterSpacing,
  );
}

TextStyle getRegularArialStyle({
  required double fontSize,
  required Color fontColor,
  TextDecoration textDecoration = TextDecoration.none,
  double letterSpacing = 0,
}) {
  return getArialStyle(
    fontWeight: AppFontWeights.regularWeight,
    fontSize: fontSize,
    fontColor: fontColor,
    textDecoration: textDecoration,
    letterSpacing: letterSpacing,
  );
}

TextStyle getMediumArialStyle({
  required double fontSize,
  required Color fontColor,
  TextDecoration textDecoration = TextDecoration.none,
  double letterSpacing = 0,
}) {
  return getArialStyle(
    fontWeight: AppFontWeights.mediumWeight,
    fontSize: fontSize,
    fontColor: fontColor,
    textDecoration: textDecoration,
    letterSpacing: letterSpacing,
  );
}

TextStyle getSemiBoldArialStyle({
  required double fontSize,
  required Color fontColor,
  TextDecoration textDecoration = TextDecoration.none,
  double letterSpacing = 0,
}) {
  return getArialStyle(
    fontWeight: AppFontWeights.semiBoldWeight,
    fontSize: fontSize,
    fontColor: fontColor,
    textDecoration: textDecoration,
    letterSpacing: letterSpacing,
  );
}

TextStyle getBoldArialStyle({
  required double fontSize,
  required Color fontColor,
  TextDecoration textDecoration = TextDecoration.none,
  double letterSpacing = 0,
}) {
  return getArialStyle(
    fontWeight: AppFontWeights.boldWeight,
    fontSize: fontSize,
    fontColor: fontColor,
    textDecoration: textDecoration,
    letterSpacing: letterSpacing,
  );
}
