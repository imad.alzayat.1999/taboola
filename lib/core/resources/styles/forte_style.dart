import 'package:flutter/material.dart';

import '../app_fonts.dart';

TextStyle getStyle({
  required double fontSize,
  required Color fontColor,
  required FontWeight fontWeight,
  required TextDecoration textDecoration,
}) {
  return TextStyle(
    fontFamily: AppFontFamilies.forteFontFamily,
    fontSize: fontSize,
    fontWeight: fontWeight,
    color: fontColor,
    decoration: textDecoration,
  );
}

TextStyle getLightForteStyle({
  required double fontSize,
  required Color fontColor,
  TextDecoration textDecoration = TextDecoration.none,
}) {
  return getStyle(
      fontWeight: AppFontWeights.lightWeight,
      fontSize: fontSize,
      fontColor: fontColor,
      textDecoration: textDecoration
  );
}

TextStyle getRegularForteStyle({
  required double fontSize,
  required Color fontColor,
  TextDecoration textDecoration = TextDecoration.none,
}) {
  return getStyle(
      fontWeight: AppFontWeights.regularWeight,
      fontSize: fontSize,
      fontColor: fontColor,
      textDecoration: textDecoration
  );
}


TextStyle getMediumForteStyle({
  required double fontSize,
  required Color fontColor,
  TextDecoration textDecoration = TextDecoration.none,
}) {
  return getStyle(
      fontWeight: AppFontWeights.mediumWeight,
      fontSize: fontSize,
      fontColor: fontColor,
      textDecoration: textDecoration
  );
}


TextStyle getSemiBoldForteStyle({
  required double fontSize,
  required Color fontColor,
  TextDecoration textDecoration = TextDecoration.none,
}) {
  return getStyle(
      fontWeight: AppFontWeights.semiBoldWeight,
      fontSize: fontSize,
      fontColor: fontColor,
      textDecoration: textDecoration
  );
}


TextStyle getBoldForteStyle({
  required double fontSize,
  required Color fontColor,
  TextDecoration textDecoration = TextDecoration.none,
}) {
  return getStyle(
      fontWeight: AppFontWeights.boldWeight,
      fontSize: fontSize,
      fontColor: fontColor,
      textDecoration: textDecoration
  );
}
