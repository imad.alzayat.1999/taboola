import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_routes.dart';
import 'package:ui_design/pages/address_page/views/address_page.dart';
import 'package:ui_design/pages/category_page/views/category_page.dart';
import 'package:ui_design/pages/details_page/views/details_page.dart';
import 'package:ui_design/pages/home_page/views/home_page.dart';
import 'package:ui_design/pages/login_page/views/login_page.dart';
import 'package:ui_design/pages/register_page/views/register_page.dart';
import 'package:ui_design/pages/settings_page/views/settings_page.dart';
import 'package:ui_design/pages/splash_page/view/splash_page.dart';

import '../../../pages/main_page/main_page.dart';

class RouteConfig {
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case AppRoutes.splashRoute:
        return MaterialPageRoute(builder: (_) => const SplashPage());
      case AppRoutes.loginRoute:
        return MaterialPageRoute(builder: (_) => const LoginPage());
      case AppRoutes.registerRoute:
        return MaterialPageRoute(builder: (_) => const RegisterPage());
      case AppRoutes.mainRoute:
        return MaterialPageRoute(builder: (_) => const MainPage());
      case AppRoutes.categoryRoute:
        return MaterialPageRoute(builder: (_) => const CategoryPage());
      case AppRoutes.homeRoute:
        return MaterialPageRoute(builder: (_) => const HomePage());
      case AppRoutes.detailsRoute:
        return MaterialPageRoute(builder: (_) => const DetailsPage());
      case AppRoutes.settingsRoute:
        return MaterialPageRoute(builder: (_) => const SettingsPage());
      case AppRoutes.addressRoute:
        return MaterialPageRoute(builder: (_) => const AddressPage());
      default:
        return MaterialPageRoute(
          builder: (_) => const Scaffold(
            body: Center(
              child: Text("No Route Found"),
            ),
          ),
        );
    }
  }
}
