import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_routes.dart';
import 'package:ui_design/pages/category_page/views/category_page.dart';
import 'package:ui_design/pages/home_page/views/home_page.dart';
import 'package:ui_design/pages/login_page/views/login_page.dart';
import 'package:ui_design/pages/menu_page/views/menu_page.dart';
import 'package:ui_design/pages/more_page/views/more_page.dart';
import 'package:ui_design/pages/profile_page/views/profile_page.dart';
import 'package:ui_design/pages/register_page/views/register_page.dart';
import 'package:ui_design/pages/splash_page/view/splash_page.dart';
import 'package:ui_design/pages/wishlist_page/views/wishlist_page.dart';

import 'core/config/route_config/route_config.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: AppRoutes.splashRoute,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      onGenerateRoute: RouteConfig.onGenerateRoute,
    );
  }
}