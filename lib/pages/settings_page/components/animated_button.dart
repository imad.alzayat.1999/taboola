import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';

class AnimatedButton extends StatelessWidget {
  final String btnText;
  final bool isActivated;
  final void Function()? onPressFunction;

  const AnimatedButton({
    Key? key,
    required this.btnText,
    required this.isActivated,
    required this.onPressFunction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressFunction,
      style: ElevatedButton.styleFrom(
        backgroundColor: isActivated ? AppColors.kWageningenGreen : AppColors.kWhite,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(AppSizes.s5),
        ),
      ),
      child: Text(
        btnText,
        style: getRegularArialStyle(
          fontSize: AppFontSizes.f14,
          fontColor: isActivated ? AppColors.kWhite : AppColors.kBlack,
        ),
      ),
    );
  }
}
