import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:ui_design/core/widgets/background_widget.dart';
import 'package:ui_design/pages/settings_page/components/divider_section.dart';

import '../../../core/resources/app_assets.dart';
import '../components/animated_button.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool _isSwitched = false;
  bool _isNotificationSwitched = false;

  bool _isArabicActivated = false;
  bool _isEnglishActivated = false;

  bool _isChangePasswordActivated = false;
  bool _isNewPasswordActivated = false;

  _toggleSwitch(bool value) {
    if (_isSwitched == false) {
      setState(() {
        _isSwitched = true;
      });
    } else {
      setState(() {
        _isSwitched = false;
      });
    }
  }

  _toggleNotificationSwitch(bool value) {
    if (_isNotificationSwitched == false) {
      setState(() {
        _isNotificationSwitched = true;
      });
    } else {
      setState(() {
        _isNotificationSwitched = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      appBar: AppBar(
        backgroundColor: AppColors.kTransparent,
        elevation: AppSizes.s0,
        shape: const Border(bottom: BorderSide(color: AppColors.kDartGreen)),
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: SizedBox(
            width: AppSizes.s24,
            height: AppSizes.s24,
            child: SvgPicture.asset(AppIcons.arrowLeftIcon),
          ),
        ),
        centerTitle: true,
        title: Text(
          AppStrings.settingsString,
          style: getRegularArialStyle(
            fontSize: AppFontSizes.f20,
            fontColor: AppColors.kDartGreen,
          ),
        ),
      ),
      child: Container(
        width: double.infinity,
        height: AppSizes.s320,
        decoration: BoxDecoration(
          color: AppColors.kIsabelline.withOpacity(0.6),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: AppPaddings.p13),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppStrings.dartModeString,
                    style: getBoldArialStyle(
                      fontSize: AppFontSizes.f16,
                      fontColor: AppColors.kBlack,
                    ),
                  ),
                  Switch(
                    value: _isSwitched,
                    activeColor: AppColors.kWageningenGreen,
                    onChanged: _toggleSwitch,
                  ),
                ],
              ),
              DividerSection(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppStrings.notificationString,
                    style: getBoldArialStyle(
                      fontSize: AppFontSizes.f16,
                      fontColor: AppColors.kBlack,
                    ),
                  ),
                  Switch(
                    value: _isNotificationSwitched,
                    activeColor: AppColors.kWageningenGreen,
                    onChanged: _toggleNotificationSwitch,
                  ),
                ],
              ),
              DividerSection(),
              Text(
                AppStrings.languageString,
                style: getBoldArialStyle(
                  fontSize: AppFontSizes.f16,
                  fontColor: AppColors.kBlack,
                ),
              ),
              SizedBox(height: AppSizes.s20),
              Row(
                children: [
                  AnimatedButton(
                    btnText: AppStrings.arabicString,
                    isActivated: _isArabicActivated,
                    onPressFunction: () {
                      setState(() {
                        _isArabicActivated = !_isArabicActivated;
                      });
                    },
                  ),
                  SizedBox(width: AppSizes.s30),
                  AnimatedButton(
                    btnText: AppStrings.englishString,
                    isActivated: _isEnglishActivated,
                    onPressFunction: () {
                      setState(() {
                        _isEnglishActivated = !_isEnglishActivated;
                      });
                    },
                  ),
                ],
              ),
              DividerSection(),
              Text(
                AppStrings.passwordSettings,
                style: getBoldArialStyle(
                  fontSize: AppFontSizes.f16,
                  fontColor: AppColors.kBlack,
                ),
              ),
              SizedBox(height: AppSizes.s20),
              Row(
                children: [
                  AnimatedButton(
                    btnText: AppStrings.changePasswordString,
                    isActivated: _isChangePasswordActivated,
                    onPressFunction: () {
                      setState(() {
                        _isChangePasswordActivated = !_isChangePasswordActivated;
                      });
                    },
                  ),
                  SizedBox(width: AppSizes.s30),
                  AnimatedButton(
                    btnText: AppStrings.newPasswordString,
                    isActivated: _isNewPasswordActivated,
                    onPressFunction: () {
                      setState(() {
                        _isNewPasswordActivated = !_isNewPasswordActivated;
                      });
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
