import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_consts.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:carousel_slider/carousel_slider.dart';

class CarouselSection extends StatefulWidget {
  const CarouselSection({Key? key}) : super(key: key);

  @override
  State<CarouselSection> createState() => _CarouselSectionState();
}

class _CarouselSectionState extends State<CarouselSection> {
  final _carouselController = CarouselController();
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: AppSizes.s180,
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: Text(
              AppStrings.viewAllString,
              style: getRegularArialStyle(
                fontSize: AppFontSizes.f12,
                fontColor: AppColors.kWageningenGreen,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                onTap: () {
                  _carouselController.previousPage();
                },
                child: Container(
                  width: AppSizes.s24,
                  height: AppSizes.s24,
                  decoration: BoxDecoration(
                    color: AppColors.kDartGreen,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(Icons.arrow_back_ios_new,
                      color: AppColors.kWhite, size: 12),
                ),
              ),
              SizedBox(
                width: AppSizes.s220,
                child: CarouselSlider(
                  items: AppConsts.images.map((e) => _getImage(e)).toList(),
                  carouselController: _carouselController,
                  options: CarouselOptions(
                    height: AppSizes.s141,
                    initialPage: 0,
                    scrollPhysics: const NeverScrollableScrollPhysics(),
                    autoPlayInterval: Duration(seconds: 3),
                    autoPlayAnimationDuration: Duration(milliseconds: 800),
                    autoPlayCurve: Curves.fastOutSlowIn,
                    enlargeCenterPage: true,
                    enlargeFactor: 0.8,
                    onPageChanged: (index, reason) {
                      setState(() {
                        _currentIndex = index;
                      });
                    },
                    scrollDirection: Axis.horizontal,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  _carouselController.nextPage();
                },
                child: Container(
                  width: AppSizes.s24,
                  height: AppSizes.s24,
                  decoration: BoxDecoration(
                    color: AppColors.kDartGreen,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(Icons.arrow_forward_ios_outlined,
                      color: AppColors.kWhite, size: 12),
                ),
              ),
            ],
          ),
          SizedBox(height: AppSizes.s7),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: AppConsts.images.asMap().entries.map((entry) {
              return InkWell(
                onTap: () => _carouselController.animateToPage(entry.key),
                child: AnimatedContainer(
                  duration: const Duration(
                    milliseconds: AppConsts.durationForIndicator,
                  ),
                  margin: const EdgeInsets.symmetric(horizontal: AppMargins.m3),
                  height: AppSizes.s12,
                  width: AppSizes.s12,
                  decoration: BoxDecoration(
                    color: _currentIndex == entry.key
                        ? AppColors.kRed
                        : AppColors.kTransparent,
                    border: Border.all(color: AppColors.kRed),
                    shape: BoxShape.circle,
                  ),
                ),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }

  Widget _getImage(String image) {
    return Container(
      width: AppSizes.s323,
      height: AppSizes.s141,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage(image),
        ),
      ),
    );
  }
}
