import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:ui_design/core/resources/styles/forte_style.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ReviewsFromCustomers extends StatelessWidget {
  const ReviewsFromCustomers({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              AppStrings.reviewsFromCustomers,
              style: getRegularForteStyle(
                fontSize: AppFontSizes.f14,
                fontColor: AppColors.kDartGreen,
              ),
            ),
            Text(
              AppStrings.viewAllString,
              style: getRegularArialStyle(
                fontSize: AppFontSizes.f12,
                fontColor: AppColors.kWageningenGreen,
              ),
            )
          ],
        ),
        SizedBox(height: AppSizes.s5),
        SizedBox(
          height: AppSizes.s100,
          child: ListView.separated(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) => Container(
              width: AppSizes.s113,
              decoration: BoxDecoration(
                color: AppColors.kIsabelline,
                borderRadius: BorderRadius.circular(AppSizes.s17),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: AppPaddings.p8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: AppSizes.s36,
                          height: AppSizes.s36,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage(AppImages.profileImage),
                            ),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            RatingBar.builder(
                              initialRating: 3,
                              itemSize: 10,
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemPadding: const EdgeInsets.symmetric(
                                horizontal: AppPaddings.p0,
                              ),
                              itemBuilder: (context, _) => const Icon(
                                Icons.emoji_emotions_sharp,
                                color: AppColors.kRed,
                              ),
                              onRatingUpdate: (rating) {
                                print(rating);
                              },
                            ),
                            Text(
                              "One Month Ago",
                              style: getRegularArialStyle(
                                fontSize: AppFontSizes.f8,
                                fontColor: AppColors.kDartGreen,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: AppSizes.s10),
                    Text(
                      "Lorem ipsum dolor sit amet, consectetaur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                      textAlign: TextAlign.center,
                      style: getRegularArialStyle(
                        fontSize: AppFontSizes.f8,
                        fontColor: AppColors.kBlackOlive,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            separatorBuilder: (context, index) =>
                const SizedBox(width: AppSizes.s20),
            itemCount: 10,
          ),
        )
      ],
    );
  }
}
