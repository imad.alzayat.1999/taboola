import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_routes.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:ui_design/core/resources/styles/forte_style.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class RecommendedForYou extends StatelessWidget {
  const RecommendedForYou({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              AppStrings.recommendedForYou,
              style: getRegularForteStyle(
                fontSize: AppFontSizes.f14,
                fontColor: AppColors.kDartGreen,
              ),
            ),
            Text(
              AppStrings.viewAllString,
              style: getRegularArialStyle(
                fontSize: AppFontSizes.f12,
                fontColor: AppColors.kWageningenGreen,
              ),
            ),
          ],
        ),
        SizedBox(height: AppSizes.s10),
        SizedBox(
          height: AppSizes.s143,
          child: ListView.separated(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) => GestureDetector(
              onTap: () => Navigator.pushNamed(context, AppRoutes.detailsRoute),
              child: Container(
                width: AppSizes.s105,
                decoration: BoxDecoration(
                  color: AppColors.kIsabelline,
                  borderRadius: BorderRadius.circular(AppSizes.s2),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: AppSizes.s104,
                      child: Image.asset(
                        AppImages.profileImage,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(height: AppSizes.s5),
                    Padding(
                      padding:
                          const EdgeInsets.symmetric(horizontal: AppPaddings.p4),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Item name",
                            style: getRegularArialStyle(
                              fontSize: AppFontSizes.f10,
                              fontColor: AppColors.kWageningenGreen,
                            ),
                          ),
                          Row(
                            children: [
                              RatingBar.builder(
                                initialRating: 3,
                                itemSize: 10,
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemPadding: const EdgeInsets.symmetric(
                                  horizontal: AppPaddings.p0,
                                ),
                                itemBuilder: (context, _) => const Icon(
                                  Icons.star,
                                  color: AppColors.kRed,
                                ),
                                onRatingUpdate: (rating) {
                                  print(rating);
                                },
                              ),
                              Text(
                                "(105)",
                                style: getRegularArialStyle(
                                  fontSize: AppFontSizes.f8,
                                  fontColor: AppColors.kTaupeGray,
                                ),
                              )
                            ],
                          ),
                          Text(
                            "Description",
                            style: getRegularArialStyle(
                              fontSize: AppFontSizes.f6,
                              fontColor: AppColors.kTaupeGray,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            separatorBuilder: (context, index) => SizedBox(width: AppSizes.s30),
            itemCount: 10,
          ),
        ),
      ],
    );
  }
}
