import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:readmore/readmore.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DeliveryFeatureSection extends StatelessWidget {
  const DeliveryFeatureSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: AppSizes.s144,
          height: AppSizes.s33,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(AppSizes.s25),
            color: AppColors.kWageningenGreen,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(AppImages.backgroundImage),
            ),
          ),
          child: Center(
            child: Text(
              AppStrings.ourDeliveryFeatureString,
              style: getBoldArialStyle(
                fontSize: AppFontSizes.f10,
                fontColor: AppColors.kWhite,
              ),
            ),
          ),
        ),
        SizedBox(height: AppSizes.s20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: AppSizes.s180,
              child: ReadMoreText(
                'Flutter is Google’s mobile UI open source framework to build high-quality native (super fast) interfaces for iOS and Android apps with the unified codebase.',
                trimLines: 2,
                trimMode: TrimMode.Line,
                trimCollapsedText: 'Show More',
                trimExpandedText: 'Show less',
                moreStyle: getBoldArialStyle(
                    fontSize: AppFontSizes.f12,
                    fontColor: AppColors.kTaupeGray),
                lessStyle: getBoldArialStyle(
                    fontSize: AppFontSizes.f12,
                    fontColor: AppColors.kTaupeGray),
                style: getRegularArialStyle(fontSize: AppFontSizes.f10, fontColor: AppColors.kTaupeGray,),
              ),
            ),
            SizedBox(
              width: AppSizes.s132,
              height: AppSizes.s132,
              child: SvgPicture.asset(AppImages.deliverImage),
            ),
          ],
        )
      ],
    );
  }
}
