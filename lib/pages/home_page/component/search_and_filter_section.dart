import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../core/resources/styles/arial_style.dart';

class SearchAndFilterSection extends StatelessWidget {
  const SearchAndFilterSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          width: AppSizes.s295,
          height: AppSizes.s42,
          child: TextFormField(
            controller: TextEditingController(),
            decoration: InputDecoration(
              hintText: AppStrings.searchFoodString,
              hintStyle: getRegularArialStyle(
                fontSize: AppFontSizes.f12,
                fontColor: AppColors.kTaupeGray,
              ),
              fillColor: AppColors.kIsabelline,
              filled: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(AppSizes.s19),
                borderSide: BorderSide.none,
              ),
              prefixIcon: Icon(Icons.search , color: AppColors.kRed),
            ),
          ),
        ),
        SizedBox(
          width: AppSizes.s20,
          height: AppSizes.s20,
          child: SvgPicture.asset(AppIcons.filterIcon , color: AppColors.kRed),
        )
      ],
    );
  }
}
