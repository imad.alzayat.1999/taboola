import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_routes.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/forte_style.dart';

import '../../../core/resources/styles/arial_style.dart';

class CategoriesSection extends StatelessWidget {
  const CategoriesSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              AppStrings.categoriesString,
              style: getRegularForteStyle(
                fontSize: AppFontSizes.f14,
                fontColor: AppColors.kDartGreen,
              ),
            ),
            Text(
              AppStrings.viewAllString,
              style: getRegularArialStyle(
                  fontColor: AppColors.kWageningenGreen,
                  fontSize: AppFontSizes.f12),
            ),
          ],
        ),
        SizedBox(height: AppSizes.s5),
        SizedBox(
          height: AppSizes.s100,
          child: ListView.separated(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) => GestureDetector(
              onTap: () => Navigator.pushNamed(context, AppRoutes.categoryRoute),
              child: Container(
                width: AppSizes.s81,
                height: AppSizes.s100,
                decoration: BoxDecoration(
                  color: AppColors.kIsabelline,
                  borderRadius: BorderRadius.circular(AppSizes.s5),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: AppSizes.s60,
                      child: Image.asset(AppImages.profileImage),
                    ),
                    SizedBox(height: AppSizes.s5),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: AppPaddings.p4),
                      child: Text(
                        "Item Name",
                        style: getRegularArialStyle(
                          fontSize: AppFontSizes.f10,
                          fontColor: AppColors.kWageningenGreen,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            separatorBuilder: (context, index) =>
                const SizedBox(width: AppSizes.s25),
            itemCount: 10,
          ),
        )
      ],
    );
  }
}
