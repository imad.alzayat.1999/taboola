import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/widgets/background_widget.dart';
import 'package:ui_design/pages/home_page/component/carousel_section.dart';
import 'package:ui_design/pages/home_page/component/categories_section.dart';
import 'package:ui_design/pages/home_page/component/delivery_feature_section.dart';
import 'package:ui_design/pages/home_page/component/recommended_for_you_section.dart';
import 'package:ui_design/pages/home_page/component/reviews_from_customers_section.dart';
import 'package:ui_design/pages/home_page/component/search_and_filter_section.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      appBar: null,
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: AppPaddings.p13),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SearchAndFilterSection(),
                SizedBox(height: AppSizes.s25),
                CarouselSection(),
                SizedBox(height: AppSizes.s10),
                CategoriesSection(),
                SizedBox(height: AppSizes.s10),
                RecommendedForYou(),
                SizedBox(height: AppSizes.s10),
                ReviewsFromCustomers(),
                SizedBox(height: AppSizes.s25),
                DeliveryFeatureSection(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
