import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';

class TitleSection extends StatelessWidget {
  const TitleSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: AppSizes.s160,
      decoration: BoxDecoration(
        color: AppColors.kWageningenGreen.withOpacity(0.4),
      ),
      child: Center(
        child: Text(
          "APPETIZERS",
          style: getBoldArialStyle(
            fontSize: AppFontSizes.f30,
            fontColor: AppColors.kWhite,
          ),
        ),
      ),
    );
  }
}
