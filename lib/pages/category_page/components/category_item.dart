import 'package:flutter/material.dart';

import '../../../core/resources/app_assets.dart';
import '../../../core/resources/app_colors.dart';
import '../../../core/resources/app_fonts.dart';
import '../../../core/resources/app_values.dart';
import '../../../core/resources/styles/arial_style.dart';



class CategoryItem extends StatelessWidget {
  const CategoryItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: AppSizes.s68,
      child: Row(
        children: [
          Container(
            height: AppSizes.s55,
            width: AppSizes.s55,
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(AppImages.profileImage),
                )),
          ),
          SizedBox(width: AppSizes.s15),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Uncle Louie’s Pizza",
                  style: getRegularArialStyle(
                    fontSize: AppFontSizes.f14,
                    fontColor: AppColors.kWageningenGreen,
                  ),
                ),
                SizedBox(height: AppSizes.s10),
                Text(
                  "Pizza, Italian, Fast, Casual.",
                  style: getRegularArialStyle(
                    fontSize: AppFontSizes.f12,
                    fontColor: AppColors.kTaupeGray,
                  ),
                ),
                Text(
                  "Price",
                  style: getRegularArialStyle(
                    fontSize: AppFontSizes.f12,
                    fontColor: AppColors.kDartGreen,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
