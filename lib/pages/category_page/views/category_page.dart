import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/widgets/background_widget.dart';
import 'package:ui_design/pages/category_page/components/category_item.dart';
import 'package:ui_design/pages/category_page/components/title_section.dart';

import '../../../core/resources/app_assets.dart';
import '../../../core/resources/app_values.dart';
import '../../../core/resources/styles/arial_style.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CategoryPage extends StatefulWidget {
  const CategoryPage({Key? key}) : super(key: key);

  @override
  State<CategoryPage> createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: AppSizes.s95),
            TitleSection(),
            ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: AppPaddings.p13),
              itemBuilder: (context, index) => CategoryItem(),
              itemCount: 10,
              separatorBuilder: (BuildContext context, int index) {
                return Divider(color: AppColors.kGainsboro, thickness: 2);
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(
            Icons.arrow_back,
            color: AppColors.kDartGreen,
          ),
        ),
        backgroundColor: AppColors.kWhite,
        elevation: AppSizes.s0,
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Tab",
              style: getBoldArialStyle(
                fontSize: AppFontSizes.f28,
                fontColor: AppColors.kWageningenGreen,
              ),
            ),
            SizedBox(
              width: AppSizes.s18,
              height: AppSizes.s18,
              child: SvgPicture.asset(
                AppIcons.tomatoIcon,
              ),
            ),
            SizedBox(
              width: AppSizes.s18,
              height: AppSizes.s18,
              child: SvgPicture.asset(
                AppIcons.tomatoIcon,
              ),
            ),
            Text(
              "la",
              style: getBoldArialStyle(
                fontSize: AppFontSizes.f28,
                fontColor: AppColors.kWageningenGreen,
              ),
            ),
          ],
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: SizedBox(
              width: AppSizes.s24,
              height: AppSizes.s24,
              child: SvgPicture.asset(
                AppIcons.cartIcon,
                color: AppColors.kDartGreen,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
