import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:flutter_svg/flutter_svg.dart';

class WishlistItem extends StatelessWidget {
  const WishlistItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: AppSizes.s68,
      child: Row(
        children: [
          Container(
            height: AppSizes.s55,
            width: AppSizes.s55,
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(AppImages.profileImage),
                )),
          ),
          SizedBox(width: AppSizes.s15),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Uncle Louie’s Pizza",
                  style: getRegularArialStyle(
                    fontSize: AppFontSizes.f14,
                    fontColor: AppColors.kWageningenGreen,
                  ),
                ),
                SizedBox(height: AppSizes.s10),
                Text(
                  "Pizza, Italian, Fast, Casual.",
                  style: getRegularArialStyle(
                    fontSize: AppFontSizes.f12,
                    fontColor: AppColors.kTaupeGray,
                  ),
                ),
                Text(
                  "Price",
                  style: getRegularArialStyle(
                    fontSize: AppFontSizes.f12,
                    fontColor: AppColors.kDartGreen,
                  ),
                ),
              ],
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: AppSizes.s20,
                height: AppSizes.s20,
                child: SvgPicture.asset(
                  AppIcons.heartIcon,
                  color: AppColors.kRed,
                ),
              ),
              SizedBox(
                width: AppSizes.s95,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.all(AppPaddings.p8),
                    backgroundColor: AppColors.kWhite,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(AppSizes.s20),
                      side: BorderSide(color: AppColors.kWageningenGreen),
                    ),
                  ),
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: AppSizes.s16,
                        height: AppSizes.s16,
                        child: SvgPicture.asset(
                          AppIcons.plusIcon,
                          color: AppColors.kWageningenGreen,
                        ),
                      ),
                      SizedBox(width: AppSizes.s2),
                      Text(
                        "Add To Cart",
                        style: getBoldArialStyle(
                          fontSize: AppFontSizes.f10,
                          fontColor: AppColors.kDartGreen,
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
