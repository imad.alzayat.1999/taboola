import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/widgets/background_widget.dart';
import 'package:ui_design/pages/wishlist_page/components/wishlist_item.dart';

class WishListPage extends StatelessWidget {
  const WishListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        SizedBox(height: AppSizes.s10),
        ListView.separated(
          shrinkWrap: true,
          padding: EdgeInsets.symmetric(horizontal: AppPaddings.p13),
          itemBuilder: (context, index) => WishlistItem(),
          physics: const NeverScrollableScrollPhysics(),
          itemCount: 10,
          separatorBuilder: (BuildContext context, int index) {
            return Divider(color: AppColors.kGainsboro, thickness: 2);
          },
        ),
      ],
    );
  }
}
