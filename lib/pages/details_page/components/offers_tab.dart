import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/forte_style.dart';

import '../../../core/resources/styles/arial_style.dart';

class OffersTab extends StatelessWidget {
  const OffersTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Lorem ipsum, Dolor sit, Amet consectetur",
          style: getBoldArialStyle(
            fontSize: AppFontSizes.f12,
            fontColor: AppColors.kBlackOlive,
          ),
        ),
        SizedBox(height: AppSizes.s20),
        SizedBox(
          width: AppSizes.s130,
          height: AppSizes.s30,
          child: MaterialButton(
            onPressed: () {},
            color: AppColors.kWhite,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: AppColors.kMaximumRed),
              borderRadius: BorderRadius.circular(AppSizes.s15)
            ),
            child: Text(
              AppStrings.getTheOrderString,
              style: getBoldArialStyle(
                fontSize: AppFontSizes.f10,
                fontColor: AppColors.kMaximumRed,
              ),
            ),
          ),
        )
      ],
    );
  }
}
