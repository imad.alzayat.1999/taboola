import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_values.dart';

import '../../../core/resources/styles/arial_style.dart';
import '../../../core/resources/styles/forte_style.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PublicDetailsSection extends StatelessWidget {
  const PublicDetailsSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "Item Name",
          style: getBoldForteStyle(
            fontSize: AppFontSizes.f14,
            fontColor: AppColors.kDartGreen,
          ),
        ),
        Row(
          children: [
            SizedBox(
              width: AppSizes.s13,
              height: AppSizes.s13,
              child: SvgPicture.asset(
                AppIcons.starIcon,
                color: AppColors.kMaximumRed,
              ),
            ),
            SizedBox(width: AppSizes.s5),
            Text(
              "4.5",
              style: getRegularArialStyle(
                fontSize: AppFontSizes.f12,
                fontColor: AppColors.kDartGreen,
              ),
            ),
          ],
        ),
        Text.rich(
          TextSpan(
            text: '50 ',
            style: getRegularArialStyle(
              fontSize: AppFontSizes.f12,
              fontColor: AppColors.kMaximumRed,
            ),
            children: <InlineSpan>[
              TextSpan(
                text: 'Points',
                style: getRegularArialStyle(
                  fontSize: AppFontSizes.f12,
                  fontColor: AppColors.kDartGreen,
                ),
              )
            ],
          ),
        ),
        Text(
          "1700",
          style: getBoldForteStyle(
            fontSize: AppFontSizes.f14,
            fontColor: AppColors.kMaximumRed,
          ),
        ),
      ],
    );
  }
}
