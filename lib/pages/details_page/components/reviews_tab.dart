import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_consts.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:percent_indicator/percent_indicator.dart';

class ReviewsTab extends StatelessWidget {
  const ReviewsTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const NeverScrollableScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.only(top: AppPaddings.p80),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Text(
                      "4.5",
                      style: getRegularArialStyle(
                        fontSize: AppFontSizes.f26,
                        fontColor: AppColors.kBlack,
                      ),
                    ),
                    RatingBar.builder(
                      initialRating: 5,
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemSize: 12,
                      itemPadding:
                          EdgeInsets.symmetric(horizontal: AppPaddings.p0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: AppColors.kMaximumRed,
                      ),
                      onRatingUpdate: (rating) {
                        print(rating);
                      },
                    ),
                  ],
                ),
                SizedBox(
                  width: AppSizes.s167,
                  child: ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) =>
                        _getPercentWidget(AppConsts.listOfPercents[index]),
                    separatorBuilder: (context, index) =>
                        SizedBox(height: AppSizes.s10),
                    itemCount: AppConsts.listOfPercents.length,
                  ),
                ),
                SizedBox(
                  width: AppSizes.s81,
                  child: ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) =>
                        _getRatingWidget(AppConsts.listOfRatings[index]),
                    separatorBuilder: (context, index) =>
                        SizedBox(height: AppSizes.s10),
                    itemCount: AppConsts.listOfRatings.length,
                  ),
                ),
              ],
            ),
            SizedBox(height: AppSizes.s30),
            Text(
              AppStrings.reviewsFromCustomers,
              style: getRegularArialStyle(
                fontSize: AppFontSizes.f12,
                fontColor: AppColors.kDartGreen,
                letterSpacing: 2,
              ),
            ),
            SizedBox(height: AppSizes.s10),
            ListView.separated(
              shrinkWrap: true,
              itemBuilder: (context, index) => _getReviewItem(),
              physics: const NeverScrollableScrollPhysics(),
              separatorBuilder: (context, index) => Container(
                width: AppSizes.s239,
                height: AppSizes.s1,
                decoration: BoxDecoration(
                    border: Border.all(color: AppColors.kGainsboro)),
              ),
              itemCount: 4,
            ),
            SizedBox(height: AppSizes.s10),
            Center(
              child: ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                  backgroundColor: AppColors.kWhite,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(AppSizes.s0),
                    side: BorderSide(color: AppColors.kWageningenGreen),
                  ),
                ),
                child: Text(
                  AppStrings.viewAllReviewsString,
                  style: getBoldArialStyle(
                    fontSize: AppFontSizes.f8,
                    fontColor: AppColors.kWageningenGreen,
                  ),
                ),
              ),
            ),
            SizedBox(height: AppSizes.s30),
            Text(
              AppStrings.shareYourReviewString,
              style: getRegularArialStyle(
                fontSize: AppFontSizes.f12,
                fontColor: AppColors.kDartGreen,
                letterSpacing: 2,
              ),
            ),
            SizedBox(height: AppSizes.s10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppStrings.reviewQuestionString,
                  style: getRegularArialStyle(
                    fontSize: AppFontSizes.f12,
                    fontColor: AppColors.kDartGreen,
                  ),
                ),
                SizedBox(
                  width: AppSizes.s130,
                  height: AppSizes.s30,
                  child: MaterialButton(
                    color: AppColors.kWhite,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(AppSizes.s15),
                      side: BorderSide(color: AppColors.kMaximumRed),
                    ),
                    onPressed: () {},
                    child: Text(
                      AppStrings.addReviewString,
                      style: getBoldArialStyle(
                        fontSize: AppFontSizes.f10,
                        fontColor: AppColors.kMaximumRed,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  /// function to get percent widget for rating belong to specific recipe
  Widget _getPercentWidget(double percent) {
    return LinearPercentIndicator(
      width: AppSizes.s167,
      lineHeight: AppSizes.s12,
      percent: percent,
      progressColor: AppColors.kWageningenGreen,
      barRadius: Radius.circular(AppSizes.s20),
    );
  }

  /// function to get rating widget for rating belong to specific recipe
  Widget _getRatingWidget(double rating) {
    return RatingBar.builder(
      initialRating: rating,
      minRating: 1,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
      itemSize: 12,
      itemPadding: EdgeInsets.symmetric(horizontal: AppPaddings.p0),
      itemBuilder: (context, _) => Icon(
        Icons.star,
        color: AppColors.kMaximumRed,
      ),
      onRatingUpdate: (rating) {
        print(rating);
      },
    );
  }

  /// function to get review for a specific recipe
  Widget _getReviewItem() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: AppPaddings.p13),
      child: Row(
        children: [
          Container(
            width: AppSizes.s35,
            height: AppSizes.s35,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(AppImages.profileImage),
                )),
          ),
          SizedBox(width: AppSizes.s15),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "2 months ago",
                style: getRegularArialStyle(
                  fontSize: AppFontSizes.f8,
                  fontColor: AppColors.kPhilippineSilver,
                ),
              ),
              SizedBox(height: AppSizes.s5),
              Text(
                "User Name",
                style: getBoldArialStyle(
                  fontSize: AppFontSizes.f10,
                  fontColor: AppColors.kBlack,
                ),
              ),
            ],
          ),
          SizedBox(width: AppSizes.s25),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _getRatingWidget(5),
              SizedBox(height: AppSizes.s10),
              SizedBox(
                width: AppSizes.s117,
                child: Text(
                  AppConsts.loremIbsumString,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: getRegularArialStyle(
                    fontSize: AppFontSizes.f8,
                    fontColor: AppColors.kDartSilver,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
