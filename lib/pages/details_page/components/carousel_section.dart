import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../core/resources/app_colors.dart';
import '../../../core/resources/app_consts.dart';

class CarouselSection extends StatefulWidget {
  const CarouselSection({Key? key}) : super(key: key);

  @override
  State<CarouselSection> createState() => _CarouselSectionState();
}

class _CarouselSectionState extends State<CarouselSection> {
  int _currentIndex = 0;
  var _carouselController = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: AppSizes.s203,
          child: CarouselSlider(
            items: AppConsts.images.map((e) => _getImage(e)).toList(),
            carouselController: _carouselController,
            options: CarouselOptions(
              height: AppSizes.s141,
              initialPage: 0,
              autoPlay: true,
              scrollPhysics: const NeverScrollableScrollPhysics(),
              autoPlayInterval: Duration(seconds: 3),
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              autoPlayCurve: Curves.fastOutSlowIn,
              enlargeCenterPage: true,
              enlargeFactor: 0.6,
              onPageChanged: (index, reason) {
                setState(() {
                  _currentIndex = index;
                });
              },
              scrollDirection: Axis.horizontal,
            ),
          ),
        ),
        SizedBox(height: AppSizes.s2),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: AppConsts.images.asMap().entries.map((entry) {
            return InkWell(
              onTap: () => _carouselController.animateToPage(entry.key),
              child: AnimatedContainer(
                duration: const Duration(
                  milliseconds: AppConsts.durationForIndicator,
                ),
                margin: const EdgeInsets.symmetric(horizontal: AppMargins.m3),
                height: AppSizes.s12,
                width: AppSizes.s12,
                decoration: BoxDecoration(
                  color: _currentIndex == entry.key
                      ? AppColors.kRed
                      : AppColors.kTransparent,
                  border: Border.all(color: AppColors.kRed),
                  shape: BoxShape.circle,
                ),
              ),
            );
          }).toList(),
        ),
      ],
    );
  }

  Widget _getImage(String image) {
    return Stack(
      alignment: Alignment.topLeft,
      children: [
        SizedBox(
          width: double.infinity,
          child: Image.asset(
            AppImages.profileImage,
            fit: BoxFit.cover,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(AppPaddings.p8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppStrings.deliverTimeString,
                style: getBoldArialStyle(
                  fontSize: AppFontSizes.f10,
                  fontColor: AppColors.kTaupeGray,
                ),
              ),
              Row(
                children: [
                  SizedBox(
                    width: AppSizes.s10,
                    height: AppSizes.s10,
                    child: SvgPicture.asset(AppIcons.clockIcon , color: AppColors.kTaupeGray),
                  ),
                  SizedBox(width: AppSizes.s5),
                  Text(
                    "50 min",
                    style: getBoldArialStyle(
                      fontSize: AppFontSizes.f10,
                      fontColor: AppColors.kTaupeGray,
                    ),
                  ),
                ],
              )
            ],
          ),
        )
      ],
    );
  }
}
