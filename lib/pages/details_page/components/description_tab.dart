import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_consts.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';

import '../../../core/resources/styles/arial_style.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DescriptionTab extends StatefulWidget {
  const DescriptionTab({Key? key}) : super(key: key);

  @override
  State<DescriptionTab> createState() => _DescriptionTabState();
}

class _DescriptionTabState extends State<DescriptionTab> {
  String? _selectedChoice1;
  String? _selectedChoice2;
  int _number = 0;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const NeverScrollableScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.only(top: AppPaddings.p80),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              AppConsts.dummyDescription,
              style: getRegularArialStyle(
                fontSize: AppFontSizes.f12,
                fontColor: AppColors.kTaupeGray,
              ),
            ),
            SizedBox(height: AppSizes.s30),
            Text(
              AppStrings.ingredients.toUpperCase(),
              style: getRegularArialStyle(
                  fontSize: AppFontSizes.f12,
                  fontColor: AppColors.kDartGreen,
                  letterSpacing: 2),
            ),
            SizedBox(height: AppSizes.s10),
            ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) => _getIngredient(index),
              separatorBuilder: (context, index) => SizedBox(height: AppSizes.s10),
              itemCount: 4,
            ),
            SizedBox(height: AppSizes.s30),
            Text(
              AppStrings.customizeYourOrder.toUpperCase(),
              style: getRegularArialStyle(
                  fontSize: AppFontSizes.f12,
                  fontColor: AppColors.kDartGreen,
                  letterSpacing: 2),
            ),
            SizedBox(height: AppSizes.s10),
            _getDropDownButton(
              onChangeFunction: (String? newValue) {
                setState(() {
                  _selectedChoice1 = newValue;
                });
              },
              dropdownValue: _selectedChoice1,
            ),
            SizedBox(height: AppSizes.s10),
            _getDropDownButton(
              onChangeFunction: (String? newValue) {
                setState(() {
                  _selectedChoice2 = newValue;
                });
              },
              dropdownValue: _selectedChoice2,
            ),
            SizedBox(height: AppSizes.s40),
            Row(
              children: [
                Container(
                  width: AppSizes.s105,
                  height: AppSizes.s40,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(AppSizes.s20),
                    color: AppColors.kWageningenGreen,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        onPressed: () {
                          setState(() {
                            _number > 0 ? _number-- : _number;
                          });
                        },
                        icon: SvgPicture.asset(
                          AppIcons.minusIcon,
                          color: AppColors.kWhite,
                        ),
                      ),
                      Text(
                        _number.toString(),
                        style: getRegularArialStyle(
                          fontSize: AppFontSizes.f10,
                          fontColor: AppColors.kWhite,
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          setState(() {
                            _number++;
                          });
                        },
                        icon: SvgPicture.asset(
                          AppIcons.plusIcon,
                          color: AppColors.kWhite,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: AppSizes.s30),
                SizedBox(
                  width: AppSizes.s150,
                  height: AppSizes.s40,
                  child: MaterialButton(
                    onPressed: () {},
                    padding: EdgeInsets.zero,
                    color: AppColors.kWageningenGreen,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(AppSizes.s17),
                    ),
                    child: Text(
                      AppStrings.addToCartString.toUpperCase(),
                      style: getRegularArialStyle(
                        fontSize: AppFontSizes.f12,
                        fontColor: AppColors.kWhite,
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _getIngredient(int index) {
    return Row(
      children: [
        SizedBox(
          width: AppSizes.s10,
          height: AppSizes.s10,
          child: SvgPicture.asset(AppIcons.tomatoIcon),
        ),
        SizedBox(width: AppSizes.s5),
        Text(
          "ingredient ${index + 1}",
          style: getRegularArialStyle(
            fontSize: AppFontSizes.f12,
            fontColor: AppColors.kTaupeGray,
          ),
        ),
      ],
    );
  }

  Widget _getDropDownButton({
    required void Function(String?)? onChangeFunction,
    required String? dropdownValue,
  }) {
    return Container(
      height: AppSizes.s25,
      width: AppSizes.s190,
      padding: const EdgeInsets.symmetric(horizontal: AppPaddings.p8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(AppSizes.s5),
        color: AppColors.kIsabelline,
      ),
      child: DropdownButton<String?>(
        value: dropdownValue,
        isExpanded: true,
        hint: Text(AppStrings.selectASuitableTypeOfOrderString , style: getRegularArialStyle(
          fontSize: AppFontSizes.f10,
          fontColor: AppColors.kBlackOlive,
        ),),
        underline: Container(),
        style: getRegularArialStyle(
          fontSize: AppFontSizes.f10,
          fontColor: AppColors.kBlackOlive,
        ),
        items: <String>['Pizza', 'Hot Dog', 'Fried Chicken', 'Spaghetti']
            .map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(
              value,
              style: getRegularArialStyle(
                fontSize: AppFontSizes.f10,
                fontColor: AppColors.kBlackOlive,
              ),
            ),
          );
        }).toList(),
        onChanged: onChangeFunction,
      ),
    );
  }
}
