import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/widgets/background_widget.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ui_design/pages/details_page/components/carousel_section.dart';
import 'package:ui_design/pages/details_page/components/description_tab.dart';
import 'package:ui_design/pages/details_page/components/offers_tab.dart';
import 'package:ui_design/pages/details_page/components/reviews_tab.dart';

import '../../../core/resources/styles/arial_style.dart';
import '../components/public_details_section.dart';

class DetailsPage extends StatefulWidget {
  const DetailsPage({Key? key}) : super(key: key);

  @override
  State<DetailsPage> createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      appBar: AppBar(
        backgroundColor: AppColors.kTransparent,
        elevation: 0,
        shape: Border(bottom: BorderSide(color: AppColors.kDartGreen)),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: SvgPicture.asset(AppIcons.arrowLeftIcon),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(AppPaddings.p8),
            child: SvgPicture.asset(AppIcons.heartIcon),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: AppPaddings.p24),
        child: DefaultTabController(
          length: 3,
          child: NestedScrollView(
            physics: NeverScrollableScrollPhysics(),
            headerSliverBuilder: (context, isScolled) {
              return [
                SliverAppBar(
                  automaticallyImplyLeading: false,
                  backgroundColor: Colors.transparent,
                  expandedHeight: AppSizes.s295,
                  collapsedHeight: AppSizes.s295,
                  flexibleSpace: _getHeader(),
                ),
                SliverPersistentHeader(
                  delegate: MyDelegate(
                    TabBar(
                      unselectedLabelStyle: getRegularArialStyle(
                        fontSize: AppFontSizes.f12,
                        fontColor: AppColors.kDartSilver,
                      ),
                      labelStyle: getBoldArialStyle(
                        fontSize: AppFontSizes.f12,
                        fontColor: AppColors.kMaximumRed,
                      ),
                      tabs: [
                        Tab(text: AppStrings.descriptionString),
                        Tab(text: AppStrings.reviewsString),
                        Tab(text: AppStrings.offersString),
                      ],
                      labelColor: AppColors.kMaximumRed,
                      unselectedLabelColor: AppColors.kDartSilver,
                      indicatorColor: AppColors.kMaximumRed,
                    ),
                  ),
                  floating: true,
                  pinned: true,
                )
              ];
            },
            body: TabBarView(children: [
              DescriptionTab(),
              ReviewsTab(),
              OffersTab(),
            ]),
          ),
        ),
      ),
    );
  }

  Widget _getHeader() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CarouselSection(),
        SizedBox(height: AppSizes.s20),
        PublicDetailsSection(),
      ],
    );
  }
}

class MyDelegate extends SliverPersistentHeaderDelegate {
  MyDelegate(this.tabBar);
  final TabBar tabBar;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: AppColors.kTransparent,
      child: tabBar,
    );
  }

  @override
  double get maxExtent => tabBar.preferredSize.height;

  @override
  double get minExtent => tabBar.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
