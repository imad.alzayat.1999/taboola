import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';

class LoginTitle extends StatelessWidget {
  const LoginTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "L",
              style: getBoldArialStyle(
                fontSize: AppFontSizes.f28,
                fontColor: AppColors.kMaximumRed,
              ),
            ),
            SizedBox(
              width: AppSizes.s18,
              height: AppSizes.s18,
              child: SvgPicture.asset(
                AppIcons.tomatoIcon,
              ),
            ),
            Text(
              "gin",
              style: getBoldArialStyle(
                fontSize: AppFontSizes.f28,
                fontColor: AppColors.kMaximumRed,
              ),
            ),
          ],
        ),
        Text(
          AppStrings.loginTitle,
          style: getBoldArialStyle(
            fontSize: AppFontSizes.f12,
            fontColor: AppColors.kDartGreen,
          ),
        ),
      ],
    );
  }
}
