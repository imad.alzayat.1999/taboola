import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoginButton extends StatelessWidget {
  final void Function()? onPressFunction;
  const LoginButton({
    Key? key,
    required this.onPressFunction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: AppSizes.s38,
      decoration: BoxDecoration(
        color: AppColors.kWhite,
        borderRadius: BorderRadius.circular(AppSizes.s19),
        border: Border.all(
          width: 2,
          color: AppColors.kWageningenGreen,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: AppPaddings.p13),
            child: Text(
              AppStrings.loginString,
              style: getBoldArialStyle(
                fontSize: AppFontSizes.f14,
                fontColor: AppColors.kWageningenGreen,
              ),
            ),
          ),
          GestureDetector(
            onTap: onPressFunction,
            child: Container(
              width: AppSizes.s87,
              height: AppSizes.s38,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(AppSizes.s15),
                color: AppColors.kWageningenGreen,
              ),
              child: Center(
                child: SizedBox(
                  width: AppSizes.s31,
                  height: AppSizes.s31,
                  child: SvgPicture.asset(
                    AppIcons.arrowRightIcon,
                    color: AppColors.kWhite,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
