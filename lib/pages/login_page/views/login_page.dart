import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_routes.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/widgets/background_widget.dart';
import 'package:ui_design/core/widgets/base_text_field_widget.dart';
import 'package:ui_design/core/widgets/logo_widget.dart';
import 'package:ui_design/pages/login_page/components/login_button.dart';
import 'package:ui_design/pages/login_page/components/login_title.dart';

import '../../../core/resources/app_values.dart';
import '../../../core/resources/styles/arial_style.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var _emailController = TextEditingController();
  var _passController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      appBar: null,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: AppPaddings.p24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            LogoWidget(),
            LoginTitle(),
            SizedBox(height: AppSizes.s40),
            BaseTextFieldWidget(
              textEditingController: _emailController,
              hintText: AppStrings.emailString,
              icon: AppIcons.emailIcon,
              obsText: false,
            ),
            SizedBox(height: AppSizes.s20),
            BaseTextFieldWidget(
              textEditingController: _passController,
              hintText: AppStrings.passwordString,
              icon: AppIcons.keyIcon,
              obsText: true,
            ),
            SizedBox(height: AppSizes.s10),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Forgot Password ...",
                style: getBoldArialStyle(
                  fontColor: AppColors.kDartGreen,
                  fontSize: AppFontSizes.f12,
                ),
              ),
            ),
            SizedBox(height: AppSizes.s50),
            LoginButton(onPressFunction: () {
              Navigator.pushNamedAndRemoveUntil(context, AppRoutes.mainRoute, (route) => false);
            }),
            SizedBox(height: AppSizes.s24),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: AppSizes.s65,
                  height: AppSizes.s1,
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.kTaupeGray, width: 1),
                  ),
                ),
                SizedBox(width: AppSizes.s7),
                Text(
                  "Or",
                  style: getRegularArialStyle(
                    fontSize: AppFontSizes.f12,
                    fontColor: AppColors.kOnyx,
                  ),
                ),
                SizedBox(width: AppSizes.s7),
                Container(
                  width: AppSizes.s65,
                  height: AppSizes.s1,
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.kTaupeGray, width: 1),
                  ),
                ),
              ],
            ),
            SizedBox(height: AppSizes.s32),
            TextButton(
              onPressed: (){
                Navigator.pushNamed(context, AppRoutes.registerRoute);
              },
              child: Text(
                "Don't have an \n account. Sign Up.",
                textAlign: TextAlign.center,
                style: getBoldArialStyle(
                  fontSize: AppFontSizes.f14,
                  fontColor: AppColors.kDartGreen,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
