import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SearchSection extends StatelessWidget {
  const SearchSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          width: AppSizes.s295,
          height: AppSizes.s42,
          child: TextField(
            decoration: InputDecoration(
              hintText: AppStrings.searchFoodString,
              hintStyle: getRegularArialStyle(
                fontSize: AppFontSizes.f12,
                fontColor: AppColors.kTaupeGray,
              ),
              filled: true,
              fillColor: AppColors.kIsabelline,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(AppSizes.s16),
                borderSide: BorderSide.none,
              ),
              prefixIcon: Icon(Icons.search, color: AppColors.kRed),
            ),
          ),
        ),
        SizedBox(
          width: AppSizes.s23,
          height: AppSizes.s23,
          child: SvgPicture.asset(AppIcons.filterIcon , color: AppColors.kRed,),
        )
      ],
    );
  }
}
