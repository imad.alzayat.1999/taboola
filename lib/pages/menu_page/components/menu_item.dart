import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_routes.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';

class MenuItem extends StatelessWidget {
  const MenuItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: AppSizes.s290,
      height: AppSizes.s65,
      child: Card(
        color: AppColors.kIsabelline,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(AppSizes.s27),
        ),
        elevation: 3,
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Positioned(
              left: -1 * AppSizes.s10,
              child: Container(
                width: AppSizes.s55,
                height: AppSizes.s55,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(AppImages.profileImage),
                    )),
              ),
            ),
            Positioned(
              right: -1 * AppSizes.s15,
              top: AppSizes.s10,
              child: GestureDetector(
                onTap: () => Navigator.pushNamed(context, AppRoutes.categoryRoute),
                child: SizedBox(
                  width: AppSizes.s38,
                  height: AppSizes.s38,
                  child: Card(
                    elevation: 3,
                    color: AppColors.kWhite,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(AppSizes.s290),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(AppPaddings.p4),
                      child: SizedBox(
                        width: AppSizes.s16,
                        height: AppSizes.s16,
                        child: SvgPicture.asset(
                          AppIcons.turnRightIcon,
                          color: AppColors.kRed,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              right: AppSizes.s0,
              bottom: AppSizes.s10,
              left: AppSizes.s60,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "title",
                    style: getBoldArialStyle(
                      fontSize: AppFontSizes.f14,
                      fontColor: AppColors.kDartGreen,
                    ),
                  ),
                  SizedBox(height: AppSizes.s7),
                  Text(
                    "subtitle",
                    style: getBoldArialStyle(
                      fontSize: AppFontSizes.f10,
                      fontColor: AppColors.kTaupeGray,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
