import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/widgets/background_widget.dart';
import 'package:ui_design/pages/menu_page/components/search_section.dart';

import '../../../core/resources/app_values.dart';
import '../components/menu_item.dart';

class MenuPage extends StatefulWidget {
  const MenuPage({Key? key}) : super(key: key);

  @override
  State<MenuPage> createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: AppPaddings.p13),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: AppSizes.s100),
            SearchSection(),
            SizedBox(height: AppSizes.s65),
            SizedBox(
              height: 500,
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Positioned(
                    left: -45,
                    child: Container(
                      width: AppSizes.s68,
                      height: AppSizes.s434,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(AppSizes.s25),
                        color: AppColors.kWageningenGreen
                      ),
                    ),
                  ),
                  Positioned(
                    left: 0,
                    top: 30,
                    bottom: 0,
                    child: Column(
                      children: [
                        MenuItem(),
                        SizedBox(height: AppSizes.s10),
                        MenuItem(),
                        SizedBox(height: AppSizes.s10),
                        MenuItem(),
                        SizedBox(height: AppSizes.s10),
                        MenuItem(),
                        SizedBox(height: AppSizes.s10),
                        MenuItem(),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
