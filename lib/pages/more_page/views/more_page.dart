import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:ui_design/core/resources/styles/forte_style.dart';
import 'package:ui_design/core/widgets/background_widget.dart';
import 'package:ui_design/pages/more_page/components/deliver_section.dart';
import 'package:ui_design/pages/more_page/components/more_item.dart';

class MorePage extends StatefulWidget {
  const MorePage({Key? key}) : super(key: key);

  @override
  State<MorePage> createState() => _MorePageState();
}

class _MorePageState extends State<MorePage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: AppPaddings.p13),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: AppSizes.s100),
            DeliverSection(),
            SizedBox(height: AppSizes.s68),
            SizedBox(
              height: AppSizes.s434,
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Positioned(
                    left: -1 * AppSizes.s30,
                    child: Container(
                      width: AppSizes.s43,
                      height: AppSizes.s434,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(AppSizes.s19),
                        color: AppColors.kWageningenGreen,
                      ),
                    ),
                  ),
                  Positioned(
                    left: AppSizes.s20,
                    top: AppSizes.s30,
                    child: Column(
                      children: [
                        MoreItem(),
                        SizedBox(height: AppSizes.s10),
                        MoreItem(),
                        SizedBox(height: AppSizes.s10),
                        MoreItem(),
                        SizedBox(height: AppSizes.s10),
                        MoreItem(),
                        SizedBox(height: AppSizes.s10),
                        MoreItem(),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
