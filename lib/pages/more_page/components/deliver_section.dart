import 'package:flutter/material.dart';

import '../../../core/resources/app_colors.dart';
import '../../../core/resources/app_fonts.dart';
import '../../../core/resources/app_strings.dart';
import '../../../core/resources/app_values.dart';
import '../../../core/resources/styles/arial_style.dart';
import '../../../core/resources/styles/forte_style.dart';


class DeliverSection extends StatelessWidget {
  const DeliverSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "${AppStrings.goodMorningString} name",
          style: getRegularForteStyle(
            fontSize: AppFontSizes.f14,
            fontColor: AppColors.kDartGreen,
          ),
        ),
        SizedBox(height: AppSizes.s7),
        Text(
          AppStrings.deliverInString,
          style: getBoldArialStyle(
            fontSize: AppFontSizes.f10,
            fontColor: AppColors.kTaupeGray,
          ),
        ),
        Row(
          children: [
            Text(
              "Current Location",
              style: getBoldArialStyle(
                fontSize: AppFontSizes.f12,
                fontColor: AppColors.kTaupeGray,
              ),
            ),
            Icon(
              Icons.keyboard_arrow_down_rounded,
              color: AppColors.kRed,
              size: AppSizes.s16,
            ),
          ],
        )
      ],
    );
  }
}
