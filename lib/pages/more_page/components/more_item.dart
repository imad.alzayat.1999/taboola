import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../core/resources/app_assets.dart';
import '../../../core/resources/app_routes.dart';

class MoreItem extends StatelessWidget {
  const MoreItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: AppSizes.s254,
      height: AppSizes.s65,
      child: Stack(
        alignment: Alignment.centerRight,
        clipBehavior: Clip.none,
        children: [
          Card(
            elevation: 2,
            color: AppColors.kIsabelline,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(AppSizes.s20),
            ),
            child: Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(horizontal: AppPaddings.p24),
              child: Text(
                AppStrings.paymentDetailsString,
                style: getBoldArialStyle(
                  fontSize: AppFontSizes.f14,
                  fontColor: AppColors.kDartGreen,
                ),
              ),
            ),
          ),
          Positioned(
            right: -1 * AppSizes.s15,
            child: GestureDetector(
              onTap: () => Navigator.pushNamed(context, AppRoutes.addressRoute),
              child: SizedBox(
                width: AppSizes.s38,
                height: AppSizes.s38,
                child: Card(
                  elevation: 3,
                  color: AppColors.kWhite,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(AppSizes.s290),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(AppPaddings.p4),
                    child: SizedBox(
                      width: AppSizes.s16,
                      height: AppSizes.s16,
                      child: SvgPicture.asset(
                        AppIcons.turnRightIcon,
                        color: AppColors.kRed,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
