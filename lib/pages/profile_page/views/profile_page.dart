import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:ui_design/pages/profile_page/components/delivering_section.dart';
import 'package:ui_design/pages/profile_page/components/edit_button.dart';
import 'package:ui_design/pages/profile_page/components/edit_button_second.dart';
import 'package:ui_design/pages/profile_page/components/info_section.dart';

import '../../../core/widgets/background_widget.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: AppPaddings.p24),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          DeliveringSection(),
          SizedBox(height: 20),
          Text(
            AppStrings.personalInformationString,
            style: getBoldArialStyle(
              fontSize: AppFontSizes.f12,
              fontColor: AppColors.kRed,
            ),
          ),
          Container(
            width: AppSizes.s81,
            height: AppSizes.s1,
            decoration: BoxDecoration(
              border: Border.all(width: 2 , color: AppColors.kRed),
            ),
          ),
          SizedBox(height: AppSizes.s50),
          InfoSection(title: AppStrings.nameString),
          SizedBox(height: AppSizes.s20),
          InfoSection(title: AppStrings.emailString),
          SizedBox(height: AppSizes.s20),
          InfoSection(title: AppStrings.phoneNumberString),
          SizedBox(height: AppSizes.s20),
          EditButton(),
          SizedBox(height: AppSizes.s87),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              EditButtonSecond(text: AppStrings.editPasswordString),
              EditButtonSecond(text: AppStrings.editLocationString),
            ],
          )
        ],
      ),
    );
  }
}
