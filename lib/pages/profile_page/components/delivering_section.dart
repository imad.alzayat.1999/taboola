import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:ui_design/core/resources/styles/forte_style.dart';

class DeliveringSection extends StatelessWidget {
  const DeliveringSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 80,
          height: 80,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(AppImages.profileImage),
            ),
          ),
        ),
        SizedBox(width: AppSizes.s32),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "${AppStrings.goodMorningString} Name",
              style: getRegularForteStyle(
                fontSize: AppFontSizes.f14,
                fontColor: AppColors.kDartGreen,
              ),
            ),
            SizedBox(height: AppSizes.s15),
            Text(
              AppStrings.deliverInString,
              style: getBoldArialStyle(
                fontSize: AppFontSizes.f10,
                fontColor: AppColors.kTaupeGray,
              ),
            ),
            Text(
              "Current Location",
              style: getBoldArialStyle(
                fontSize: AppFontSizes.f10,
                fontColor: AppColors.kDartSilver,
              ),
            ),
          ],
        )
      ],
    );
  }
}
