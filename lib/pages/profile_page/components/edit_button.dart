import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';

class EditButton extends StatelessWidget {
  const EditButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 110,
      child: ElevatedButton(
        onPressed: () {},
        style: ElevatedButton.styleFrom(
          backgroundColor: AppColors.kWageningenGreen,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(AppSizes.s30),
          ),
        ),
        child: Text(
          AppStrings.editString,
          style: getBoldArialStyle(
            fontSize: AppFontSizes.f12,
            fontColor: AppColors.kWhite,
          ),
        ),
      ),
    );
  }
}
