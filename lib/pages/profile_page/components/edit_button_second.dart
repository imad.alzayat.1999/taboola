import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';

class EditButtonSecond extends StatelessWidget {
  final String text;
  const EditButtonSecond({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: AppSizes.s135,
      height: AppSizes.s25,
      decoration: BoxDecoration(
        color: AppColors.kIsabelline,
        borderRadius: BorderRadius.circular(AppSizes.s13),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: AppSizes.s10,
            height: AppSizes.s10,
            child: SvgPicture.asset(AppIcons.tomatoIcon),
          ),
          SizedBox(width: AppSizes.s7),
          Text(
            text,
            style: getRegularArialStyle(
              fontSize: AppFontSizes.f14,
              fontColor: AppColors.kRed,
            ),
          ),
        ],
      ),
    );
  }
}
