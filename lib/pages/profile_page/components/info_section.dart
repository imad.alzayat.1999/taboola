import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';

class InfoSection extends StatelessWidget {
  final String title;
  const InfoSection({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: AppSizes.s34,
        width: double.infinity,
        child: TextField(
          readOnly: true,
          decoration: InputDecoration(
            filled: true,
            fillColor: AppColors.kWhite,
            hintText: title,
            hintStyle: getBoldArialStyle(
              fontSize: AppFontSizes.f12,
              fontColor: AppColors.kWageningenGreen,
            ),
            contentPadding: EdgeInsets.symmetric(
              vertical: AppPaddings.p0,
              horizontal: AppPaddings.p13,
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(AppSizes.s23),
              borderSide:
                  BorderSide(color: AppColors.kWageningenGreen, width: 2),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(AppSizes.s23),
              borderSide:
                  const BorderSide(color: AppColors.kWageningenGreen, width: 2),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(AppSizes.s23),
              borderSide:
                  const BorderSide(color: AppColors.kWageningenGreen, width: 2),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(AppSizes.s23),
              borderSide:
                  const BorderSide(color: AppColors.kWageningenGreen, width: 2),
            ),
          ),
        ));
  }
}
