import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';

class AddressItem extends StatelessWidget {
  const AddressItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: AppSizes.s120,
      width: AppSizes.s340,
      child: Card(
        color: AppColors.kIsabelline.withOpacity(0.8),
        elevation: AppSizes.s5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(AppSizes.s25),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: AppPaddings.p24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: AppPaddings.p16),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: SizedBox(
                    width: AppSizes.s26,
                    height: AppSizes.s26,
                    child: SvgPicture.asset(AppIcons.addressIcon),
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Main Address",
                    style: getRegularArialStyle(
                      fontSize: AppFontSizes.f14,
                      fontColor: AppColors.kDartGreen,
                    ),
                  ),
                  Text(
                    "Area Name",
                    style: getRegularArialStyle(
                      fontSize: AppFontSizes.f12,
                      fontColor: AppColors.kBlack,
                    ),
                  ),
                  Text(
                    "Street: lorem ibsum",
                    style: getRegularArialStyle(
                      fontSize: AppFontSizes.f12,
                      fontColor: AppColors.kBlack,
                    ),
                  ),
                  Text(
                    "House/Building No:14",
                    style: getRegularArialStyle(
                      fontSize: AppFontSizes.f12,
                      fontColor: AppColors.kBlack,
                    ),
                  ),
                  Text(
                    "Phone no: 0956759576",
                    style: getRegularArialStyle(
                      fontSize: AppFontSizes.f12,
                      fontColor: AppColors.kBlack,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  SizedBox(
                    width: AppSizes.s50,
                    height: AppSizes.s25,
                    child: ElevatedButton(
                      onPressed: () {},
                      style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.all(AppPaddings.p0),
                        backgroundColor: AppColors.kWhite,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: AppColors.kBlack),
                          borderRadius: BorderRadius.circular(AppSizes.s13),
                        ),
                      ),
                      child: Text(
                        AppStrings.editString.toUpperCase(),
                        style: getRegularArialStyle(
                          fontSize: AppFontSizes.f12,
                          fontColor: AppColors.kBlack,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: AppSizes.s7),
                  SizedBox(
                    width: AppSizes.s50,
                    height: AppSizes.s25,
                    child: ElevatedButton(
                      onPressed: () {},
                      style: ElevatedButton.styleFrom(
                        backgroundColor: AppColors.kWhite,
                        padding: const EdgeInsets.all(AppPaddings.p0),
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: AppColors.kBlack),
                          borderRadius: BorderRadius.circular(AppSizes.s13),
                        ),
                      ),
                      child: Text(
                        AppStrings.deleteString.toUpperCase(),
                        style: getRegularArialStyle(
                          fontSize: AppFontSizes.f12,
                          fontColor: AppColors.kBlack,
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
