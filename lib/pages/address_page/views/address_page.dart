import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:ui_design/core/widgets/background_widget.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ui_design/pages/address_page/components/address_item.dart';

import '../../../core/resources/app_assets.dart';

class AddressPage extends StatefulWidget {
  const AddressPage({Key? key}) : super(key: key);

  @override
  State<AddressPage> createState() => _AddressPageState();
}

class _AddressPageState extends State<AddressPage> {
  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      appBar: AppBar(
        backgroundColor: AppColors.kTransparent,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: SvgPicture.asset(AppIcons.arrowLeftIcon),
        ),
        elevation: AppSizes.s0,
        centerTitle: true,
        title: Text(
          AppStrings.addressString,
          style: getRegularArialStyle(
            fontSize: AppFontSizes.f20,
            fontColor: AppColors.kDartGreen,
          ),
        ),
        shape: Border(bottom: BorderSide(color: AppColors.kDartGreen)),
        actions: [
          IconButton(
            onPressed: () {},
            icon: SvgPicture.asset(AppIcons.cartIcon),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: AppPaddings.p13),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: AppSizes.s100),
              Text(
                AppStrings.deliverInString,
                style: getBoldArialStyle(
                  fontSize: AppFontSizes.f10,
                  fontColor: AppColors.kTaupeGray.withOpacity(0.6),
                ),
              ),
              Text(
                "Current Location",
                style: getBoldArialStyle(
                  fontSize: AppFontSizes.f10,
                  fontColor: AppColors.kTaupeGray,
                ),
              ),
              SizedBox(height: AppSizes.s25),
              SizedBox(
                height: 500,
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    Positioned(
                      left: -1 * AppSizes.s40,
                      child: Container(
                        width: AppSizes.s68,
                        height: AppSizes.s434,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(AppSizes.s25),
                          color: AppColors.kWageningenGreen,
                        ),
                      ),
                    ),
                    Positioned(
                      left: AppSizes.s0,
                      top: AppSizes.s20,
                      child: Column(
                        children: [
                          AddressItem(),
                          SizedBox(height: AppSizes.s20),
                          AddressItem(),
                          SizedBox(height: AppSizes.s20),
                          AddressItem(),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: AppSizes.s34),
              Center(
                child: SizedBox(
                  width: AppSizes.s170,
                  height: AppSizes.s40,
                  child: ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.all(AppPaddings.p0),
                      backgroundColor: AppColors.kWageningenGreen,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(AppSizes.s20),
                      )
                    ),
                    child: Text(
                      AppStrings.addAddressString,
                      style: getBoldArialStyle(
                        fontSize: AppFontSizes.f12,
                        fontColor: AppColors.kWhite,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
