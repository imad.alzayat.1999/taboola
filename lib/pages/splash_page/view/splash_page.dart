import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_consts.dart';
import 'package:ui_design/core/resources/app_routes.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/widgets/background_widget.dart';

import '../../../core/resources/app_assets.dart';
import '../../../core/resources/app_colors.dart';
import '../../../core/resources/app_fonts.dart';
import '../../../core/resources/styles/arial_style.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _goToTheNextPage();
  }

  _goToTheNextPage(){
    Future.delayed(const Duration(seconds: AppConsts.durationForSplashScreen) , _initialRouting);
  }

  _initialRouting(){
    Navigator.pushNamedAndRemoveUntil(context, AppRoutes.loginRoute, (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      appBar: null,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(AppImages.logoImage),
          Text(
            AppStrings.foodDeliveryString,
            style: getBoldArialStyle(
              fontSize: AppFontSizes.f14,
              fontColor: AppColors.kMaximumRed,
            ),
          ),
        ],
      ),
    );
  }
}
