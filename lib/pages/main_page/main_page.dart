import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_consts.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:ui_design/core/resources/styles/forte_style.dart';
import 'package:ui_design/core/widgets/base_background_image_widget.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../core/widgets/base_drawer.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: BaseDrawer(),
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: IconThemeData(color: AppColors.kDartGreen),
        backgroundColor: AppColors.kTransparent,
        elevation: AppSizes.s0,
        centerTitle: true,
        shape: Border(bottom: BorderSide(color: AppColors.kDartGreen)),
        title: Text(
          AppConsts.titles[_currentIndex],
          style: getBoldForteStyle(
            fontSize: AppFontSizes.f24,
            fontColor: AppColors.kDartGreen,
          ),
        ),
      ),
      body: SizedBox(
        height: double.infinity,
        child: Stack(
          children: [
            BaseBackgroundImageWidget(),
            Positioned.fill(top: 0, child: AppConsts.pages[_currentIndex]),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: AppColors.kGainsboro,
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        selectedLabelStyle: getBoldArialStyle(
          fontSize: AppFontSizes.f12,
          fontColor: AppColors.kRed,
        ),
        unselectedLabelStyle: getBoldArialStyle(
          fontSize: AppFontSizes.f12,
          fontColor: AppColors.kRed,
        ),
        selectedItemColor: AppColors.kRed,
        unselectedItemColor: AppColors.kDartSilver,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        items: [
          BottomNavigationBarItem(
              icon: SizedBox(
                width: AppSizes.s16,
                height: AppSizes.s16,
                child: SvgPicture.asset(
                  AppIcons.menuIcon,
                  color: _currentIndex == 0
                      ? AppColors.kRed
                      : AppColors.kDartSilver,
                ),
              ),
              label: AppStrings.menuString
          ),
          BottomNavigationBarItem(
              icon: SizedBox(
                width: AppSizes.s16,
                height: AppSizes.s16,
                child: SvgPicture.asset(
                  AppIcons.heartIcon,
                  color: _currentIndex == 1
                      ? AppColors.kRed
                      : AppColors.kDartSilver,
                ),
              ),
              label: AppStrings.wishlistString
          ),
          BottomNavigationBarItem(
            icon: SizedBox(
              width: AppSizes.s16,
              height: AppSizes.s16,
              child: SvgPicture.asset(
                AppIcons.homeIcon,
                color:
                    _currentIndex == 2 ? AppColors.kRed : AppColors.kDartSilver,
              ),
            ),
            label: AppStrings.homeString,
          ),
          BottomNavigationBarItem(
              icon: SizedBox(
                width: AppSizes.s16,
                height: AppSizes.s16,
                child: SvgPicture.asset(
                  AppIcons.userIcon,
                  color: _currentIndex == 3
                      ? AppColors.kRed
                      : AppColors.kDartSilver,
                ),
              ),
              label: AppStrings.profileString,
          ),
          BottomNavigationBarItem(
            icon: SizedBox(
              width: AppSizes.s16,
              height: AppSizes.s16,
              child: SvgPicture.asset(
                AppIcons.moreIcon,
                color:
                    _currentIndex == 4 ? AppColors.kRed : AppColors.kDartSilver,
              ),
            ),
            label: AppStrings.moreString,
          ),
        ],
      ),
    );
  }
}
