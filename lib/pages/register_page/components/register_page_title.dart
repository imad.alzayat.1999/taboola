import 'package:flutter/material.dart';

import '../../../core/resources/app_colors.dart';
import '../../../core/resources/app_fonts.dart';
import '../../../core/resources/app_strings.dart';
import '../../../core/resources/styles/arial_style.dart';


class RegisterPageTitle extends StatelessWidget {
  const RegisterPageTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          AppStrings.signupTitle,
          style: getBoldArialStyle(
            fontSize: AppFontSizes.f28,
            fontColor: AppColors.kMaximumRed,
          ),
        ),
        Text(
          AppStrings.signupSubTitle,
          style: getBoldArialStyle(
            fontSize: AppFontSizes.f12,
            fontColor: AppColors.kDartGreen,
          ),
        ),
      ],
    );
  }
}
