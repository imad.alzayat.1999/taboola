import 'package:flutter/material.dart';
import 'package:ui_design/core/resources/app_assets.dart';
import 'package:ui_design/core/resources/app_colors.dart';
import 'package:ui_design/core/resources/app_fonts.dart';
import 'package:ui_design/core/resources/app_routes.dart';
import 'package:ui_design/core/resources/app_strings.dart';
import 'package:ui_design/core/resources/app_values.dart';
import 'package:ui_design/core/resources/styles/arial_style.dart';
import 'package:ui_design/core/widgets/background_widget.dart';
import 'package:ui_design/core/widgets/base_text_field_widget.dart';
import 'package:ui_design/core/widgets/logo_widget.dart';
import 'package:ui_design/pages/register_page/components/register_button.dart';
import 'package:ui_design/pages/register_page/components/register_page_title.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  var _nameController = TextEditingController();
  var _emailController = TextEditingController();
  var _addressController = TextEditingController();
  var _phoneController = TextEditingController();
  var _passController = TextEditingController();
  var _confirmPassController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      appBar: null,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: AppPaddings.p24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              LogoWidget(),
              RegisterPageTitle(),
              SizedBox(height: AppSizes.s40),
              BaseTextFieldWidget(
                textEditingController: _nameController,
                hintText: AppStrings.nameString,
                icon: AppIcons.userIcon,
                obsText: false,
              ),
              SizedBox(height: AppSizes.s20),
              BaseTextFieldWidget(
                textEditingController: _emailController,
                hintText: AppStrings.emailString,
                icon: AppIcons.emailIcon,
                obsText: false,
              ),
              SizedBox(height: AppSizes.s20),
              BaseTextFieldWidget(
                textEditingController: _addressController,
                hintText: AppStrings.addressString,
                icon: AppIcons.homeIcon,
                obsText: false,
              ),
              SizedBox(height: AppSizes.s20),
              BaseTextFieldWidget(
                textEditingController: _phoneController,
                hintText: AppStrings.phoneNumberString,
                icon: AppIcons.phoneIcon,
                obsText: false,
              ),
              SizedBox(height: AppSizes.s20),
              BaseTextFieldWidget(
                textEditingController: _passController,
                hintText: AppStrings.passwordString,
                icon: AppIcons.keyIcon,
                obsText: false,
              ),
              SizedBox(height: AppSizes.s20),
              BaseTextFieldWidget(
                textEditingController: _confirmPassController,
                hintText: AppStrings.confirmPassString,
                icon: AppIcons.eyeIcon,
                obsText: false,
              ),
              SizedBox(height: AppSizes.s32),
              RegisterButton(onPressFunction: (){
                Navigator.pushNamedAndRemoveUntil(context, AppRoutes.mainRoute, (route) => false);
              }),
            ],
          ),
        ),
      ),
    );
  }
}
